# Run The Code

```bash
./gradlew run --args=a_example
./gradlew run --args=b_little_bit_of_everything
./gradlew run --args=c_many_ingredients
./gradlew run --args=d_many_pizzas
./gradlew run --args=e_many_teams
```

# Scores

|Problem|Score|
|---|---|
|a_example|49|
|b_little_bit_of_everything|11,638|
|c_many_ingredients|709,173,901|
|d_many_pizzas|7,946,302|
|e_many_teams|10,872,883|
