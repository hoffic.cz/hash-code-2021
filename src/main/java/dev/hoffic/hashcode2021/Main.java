package dev.hoffic.hashcode2021;

import dev.hoffic.hashcode2021.io.Dumper;
import dev.hoffic.hashcode2021.io.Parser;
import dev.hoffic.hashcode2021.logic.Computer;

import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Stream.of("a", "b", "c", "d", "e", "f").forEach(problemName -> {

            System.out.println("Parsing...");
            var input = new Parser().parseFile(problemName);
            System.out.println(input.toString());

            System.out.println("Computing...");
            new Computer().compute(input);
            System.out.println(input);

            System.out.println("Dumping...");
            new Dumper().dump(input, problemName);
        });
    }
}
