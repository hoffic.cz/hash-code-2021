package dev.hoffic.hashcode2021.logic;

import dev.hoffic.hashcode2021.model.Input;
import dev.hoffic.hashcode2021.model.Signal;

import java.util.*;

public class Computer {

    public void compute(Input input) {
        input.cars.forEach(car -> {
            car.streets.forEach(street -> {
                street.frequency++;
            });
        });

        Arrays.stream(input.intersections).forEach(intersection -> {

            int totalCars = intersection.ingress.stream()
                    .mapToInt(street -> street.frequency).sum();

            long intersectionsWithCars = intersection.ingress.stream()
                    .filter(street -> street.frequency > 0)
                    .count();

            if (totalCars > 0) {
                intersection.ingress.stream()
                        .filter(street -> street.frequency > 0)
                        .forEach(street -> {
                            var duration = 1.0 * intersectionsWithCars * street.frequency / totalCars;

                            if (duration > 0) {
                                intersection.schedule.add(new Signal(street, Math.sqrt(duration)));
                            }
                        });
            } else {
                var street = intersection.ingress.stream().findFirst().get();
                intersection.schedule.add(new Signal(street, 1));
            }
        });
    }
}
