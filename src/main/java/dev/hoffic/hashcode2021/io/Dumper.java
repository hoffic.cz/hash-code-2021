package dev.hoffic.hashcode2021.io;

import dev.hoffic.hashcode2021.model.Input;

import java.io.PrintWriter;
import java.util.Arrays;

public class Dumper {

    public void dump(Input input, String problemName) {
        try {
            var writer = new PrintWriter("output/" + problemName + ".out");

            writer.println(input.intersections.length);

            Arrays.stream(input.intersections).forEach(intersection -> {
                writer.println(intersection.id);
                writer.println(intersection.schedule.size());

                intersection.schedule
                        .stream()
                        .sorted((signal, t1) -> t1.street.frequency - signal.street.frequency)
                        .forEach(signal -> {
                    writer.println(signal.street.name + " " + signal.duration);
                });
            });

            writer.flush();
        } catch (Exception e) {
            System.out.println("Je to v hajzlu!");
        }
    }
}
