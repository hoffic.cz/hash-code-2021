package dev.hoffic.hashcode2021.io;

import dev.hoffic.hashcode2021.model.Car;
import dev.hoffic.hashcode2021.model.Input;
import dev.hoffic.hashcode2021.model.Intersection;
import dev.hoffic.hashcode2021.model.Street;

import java.io.File;
import java.util.Scanner;

public class Parser {

    final static String PATH = "input/";

    public Input parseFile(String problem) {
        System.out.println("Parsing problem " + problem);

        Input input;

        try (var fileScanner = new Scanner(new File(PATH + problem + ".in"))) {
            var header = fileScanner.nextLine();
            var headerScanner = new Scanner(header);

            int simulationDuration = headerScanner.nextInt();
            int numberOfIntersections = headerScanner.nextInt();
            int numberOfStreets = headerScanner.nextInt();
            int numberOfCars = headerScanner.nextInt();
            int finishedBonus = headerScanner.nextInt();

            input = new Input(numberOfIntersections);
            input.simulationDuration = simulationDuration;
            input.finishedBonus = finishedBonus;

            for (int i = 0; i < numberOfIntersections; i++) {
                input.intersections[i] = new Intersection(i);
            }

            for (int i = 0; i < numberOfStreets; i++) {
                Scanner streetScanner = new Scanner(fileScanner.nextLine());

                Intersection start = input.intersections[streetScanner.nextInt()];
                Intersection end = input.intersections[streetScanner.nextInt()];
                String streetName = streetScanner.next();
                int streetLength = streetScanner.nextInt();

                var street = new Street(start, end, streetName, streetLength);

                start.egress.add(street);
                end.ingress.add(street);

                input.streets.put(streetName, street);
            }

            for (int i = 0; i < numberOfCars; i++) {
                Scanner carScanner = new Scanner(fileScanner.nextLine());
                int carStreets = carScanner.nextInt();

                Car car = new Car();

                for (int j = 0; j < carStreets; j++) {
                    car.streets.add(input.streets.get(carScanner.next()));
                }

                input.cars.add(car);
            }

            return input;
        } catch (Exception e) {
            System.out.println("Je to v prdeli!");
            return new Input(0);
        }
    }
}
