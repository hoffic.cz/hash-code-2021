package dev.hoffic.hashcode2021.model;

public class Signal {
    public Street street;
    public int duration;

    public Signal(Street street, double duration) {
        this.street = street;
        this.duration = (int) Math.round(duration);
        if (this.duration == 0) {
            this.duration = 1;
        }
    }
}
