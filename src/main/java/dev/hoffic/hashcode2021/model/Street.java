package dev.hoffic.hashcode2021.model;

import java.util.Objects;

public class Street {
    public Intersection start;
    public Intersection end;
    public String name;
    public int length;

    public int frequency = 0;

    public Street(Intersection start, Intersection end, String name, int length) {
        this.start = start;
        this.end = end;
        this.name = name;
        this.length = length;
    }

    @Override
    public boolean equals(Object o) {
        var other = (Street) o;

        return name.equals(other.name)
                && start.equals(other.start)
                && end.equals(other.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
