package dev.hoffic.hashcode2021.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Intersection {
    public int id;
    public List<Street> ingress;
    public List<Street> egress;
    public LinkedList<Signal> schedule;

    public Intersection(int id) {
        this.id = id;
        ingress = new ArrayList<>();
        egress = new ArrayList<>();
        schedule = new LinkedList<>();
    }
}
