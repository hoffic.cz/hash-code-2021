package dev.hoffic.hashcode2021.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.StringJoiner;

public class Input {

    public int simulationDuration;
    public int finishedBonus;
    public Intersection[] intersections;
    public HashMap<String, Street> streets;
    public ArrayList<Car> cars; // + bonus

    public Input(int numberOfIntersections) {
        intersections = new Intersection[numberOfIntersections];
        streets = new HashMap<>();
        cars = new ArrayList<>();
    }
}
